package main

import (
	"errors"

	melody "gopkg.in/olahol/melody.v1"
)

/*
NewApp erstellt eine neue App (unser constructor)
*/
func NewApp(m *melody.Melody) *App {
	return &App{Games: map[string]*Game{}, Melody: m}
}

/*
App verwaltet unsere laufenden spiele, und unsere websockets
*/
type App struct {
	Games  map[string]*Game
	Melody *melody.Melody
}

/*
NewGame erstellt ein neues Spiel,
oder gibt uns das vorhandene zurück,
falls es schon eines mit diesem namen gibt
*/
func (a *App) NewGame(GameName string) *Game {
	game, err := a.GameByName(GameName)
	if err == nil {
		return game
	}
	a.Games[GameName] = &Game{
		Turn: "player1",
		Values: [][]int{
			[]int{0, 0, 0},
			[]int{0, 0, 0},
			[]int{0, 0, 0},
		},
	}
	return a.Games[GameName]
}

/*
GameByName gibt uns ein spiel nach spielnamen zurück,
falls es nicht existiert - geben wir einen fehler zurück
*/
func (a *App) GameByName(GameName string) (*Game, error) {
	if game, ok := a.Games[GameName]; ok {
		return game, nil
	}
	return nil, errors.New("Game existiert nicht")
}

/*
DeleteGameByName löscht ein spiel nach namen
*/
func (a *App) DeleteGameByName(GameName string) error {
	delete(a.Games, GameName)
	return nil
}

/*
AvailableGames gibt uns die namen aller gerade laufenden spiele zurück
*/
func (a *App) AvailableGames() []string {
	keys := make([]string, 0, len(a.Games))
	for k, game := range a.Games {
		if game.IsFree() {
			keys = append(keys, k)
		}
	}
	return keys
}

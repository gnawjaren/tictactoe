package main

import (
	"encoding/json"
	"fmt"
	"strings"

	"github.com/gin-gonic/gin"
	"gitlab.com/gnawjaren/tictactoe/frontend"
	"gopkg.in/olahol/melody.v1"
)

func main() {
	// Eine instanz von melody erstellen - https://github.com/olahol/melody
	// zum verwalten unserer websockets
	m := melody.New()

	// wir erstellen ein instanz unseres "Web" structs (siehe web.go)
	// mit einer instanz unseres "App" structs (siehe app.go)
	web := Web{
		App: NewApp(m),
	}

	// unseren gin.router erstellen (https://github.com/gin-gonic/gin)
	// damit verdrahten wir unsere request handler
	r := gin.Default()

	// hier werden die routes "verkabelt" - unter der route (1. Parameter)
	// wird die methode (2. parameter) aufgerufen - diese muss die signatur func(c *gin.Context) haben
	r.GET("/api/newgame/:gamename", web.NewGame)
	r.GET("/api/gamestatus/:gamename", web.GameStatus)
	r.GET("/api/joingame/:gamename/:playername", web.JoinGame)
	r.POST("/api/gamemove/:gamename", web.MakeMove)
	r.DELETE("/api/game/:gamename", web.DeleteGame)
	r.POST("/api/leavegame/:gamename/:playername", web.LeaveGame)
	r.GET("/api/availablegames", func(c *gin.Context) {
		games := web.App.AvailableGames()

		c.JSON(200, games)
	})

	// unser websocket - wird von melody übernommen
	r.GET("/ws/:gamename/:playername", func(c *gin.Context) {
		m.HandleRequest(c.Writer, c.Request)
	})

	// im connect merken wir uns das aktuelle spiel - und den spielernamen
	m.HandleConnect(func(s *melody.Session) {

		fmt.Println("connected!")
		fmt.Println(s.Request.URL.Path)
		params := strings.Split(s.Request.URL.Path, "/")
		playerName := params[3]
		gameName := params[2]
		s.Set("playername", playerName)
		s.Set("gamename", gameName)

	})

	// im disconnect verlassen wir das spiel - und falls niemand mehr im spiel ist
	// wird dieses gelöscht
	m.HandleDisconnect(func(s *melody.Session) {
		playerName := s.MustGet("playername").(string)
		gameName := s.MustGet("gamename").(string)
		fmt.Println("Disconnected:" + playerName)
		if game, err := web.App.GameByName(gameName); err == nil {
			game.LeaveGame(&Player{playerName})
			if game.IsEmpty() {
				fmt.Println("Keiner mehr drin, lösche spiel!")
				web.App.DeleteGameByName(gameName)
			}
			var gameData []byte
			if gameData, err = json.Marshal(game); err != nil {
				return
			}
			m.BroadcastFilter(gameData, func(q *melody.Session) bool {
				game1 := strings.Split(s.Request.URL.Path, "/")[2]
				game2 := strings.Split(q.Request.URL.Path, "/")[2]
				return game1 == game2
			})
		}
	})

	// unser Broadcaster - wir senden einfach den aktuellen zug an
	// alle teilnehmer des spiels
	m.HandleMessage(func(s *melody.Session, msg []byte) {
		playerName := s.MustGet("playername").(string)
		fmt.Println("Zug von:" + playerName)
		m.BroadcastFilter(msg, func(q *melody.Session) bool {
			game1 := strings.Split(s.Request.URL.Path, "/")[2]
			game2 := strings.Split(q.Request.URL.Path, "/")[2]
			return game1 == game2
		})
	})

	// unser static files handler (für die js / css etc dateien)
	r.StaticFS("/static", frontend.StaticFiles())

	// wenn keine der obigen routes passt - zeigen wir die index file an
	r.NoRoute(frontend.IndexFile())
	// wir starten den webserver (auf port 8080)
	r.Run(":8080")
}

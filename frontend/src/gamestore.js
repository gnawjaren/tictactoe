import alt from "./alt.js"
import GameActions from "./gameactions"

class GameStore{
  constructor(){
    this.available_games = []
    this.bindListeners({
      onLoadedGames: GameActions.loadedAvailableGames,
    })
  }
  onLoadedGames(games){
    this.available_games = games
  }
}

export default alt.createStore(GameStore)
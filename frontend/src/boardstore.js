import alt from "./alt.js"
import BoardActions from "./boardactions"

class BoardStore{
  constructor(){
    this.winner = null
    this.player1 = null
    this.player2 = null
    this.turn = "player1"
    this.error = null
    this.values = [
      [0,0,0],
      [0,0,0],
      [0,0,0]
    ]
    this.bindListeners({
      onWsMove: BoardActions.onWsMove,
      onError: BoardActions.onError,
      onReset: BoardActions.resetGame,
      onLeave: BoardActions.leaveGame,
    })

  }
  onWsMove({winner, player1, player2, turn, values}){
    this.winner = winner
    this.turn = turn
    this.values = values
    this.player1 = player1
    this.player2 = player2
  }
  onError({Message}){
    this.error = Message
  }
  onReset(){
    this.winner = null
    this.player1 = null
    this.player2 = null
    this.turn = "player1"
    this.error = null
    this.values = [
      [0,0,0],
      [0,0,0],
      [0,0,0]
    ]
  }
  onLeave(){
    this.winner = null
    this.player1 = null
    this.player2 = null
    this.turn = "player1"
    this.error = null
    this.values = [
      [0,0,0],
      [0,0,0],
      [0,0,0]
    ]
  }
}

export default alt.createStore(BoardStore)
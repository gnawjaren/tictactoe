import React from "react"
import PropTypes from "prop-types"
import "./boardstyles.css"

import BoardActions from "./boardactions"
import BoardStore from "./boardstore"

import GameActions from "./gameactions"
import GameStore from "./gamestore"

const players = [
  null, "player1", "player2"
]

const TIE = 3

export function NewAppState(){
  return {
    "values":[
      [0,0,0],
      [0,0,0],
      [0,0,0]
    ],
    "player1":{
      "name":"Player1"
    },
    "player2":{
      "name":"Player2"
    },
    "winner":null,
    "turn":"player1"
  }
} 

export default class OfflineBoardController extends React.Component{
  constructor(props){
    super(props)
    this.makeMove = this.makeMove.bind(this)
    this.checkWinner = this.checkWinner.bind(this)
    this.reset = this.reset.bind(this)
    this.state = NewAppState()
  }
  reset(){
    this.setState(NewAppState())
  }
  checkWinner(values){
    //Rows prüfen
    for (var row = 0;row <= 2; row++){
      var lastValue = null
      for (var col = 0;col <= 2;col++){
        var currentValue = values[row][col]
        if(lastValue === null){
          if(currentValue === 0){
            break
          }else{
            lastValue = currentValue
          }
        }else{
          if (currentValue !== lastValue){
            break
          }
          if (col == 2){
            return currentValue
          }
        }
      }
    }
    //Cols Prüfen
    for (col = 0;col <= 2; col++){
      lastValue = null
      for (row = 0;row <= 2;row++){
        currentValue = values[row][col]
        if(lastValue === null){
          if(currentValue === 0){
            break
          }else{
            lastValue = currentValue
          }
        }else{
          if (currentValue !== lastValue){
            break
          }
          if (row == 2){
            return currentValue
          }
        }
      }
    }
    // Über kreuz
    if (values[1][1] !== 0){
      if(values[0][0] === values[1][1] && values[1][1] === values[2][2]){
        return values[1][1]
      }
      if(values[2][0] === values[1][1] && values[1][1] === values[0][2]){
        return values[1][1]
      }
    }
    // Unentschieden ?
    let foundEmpty = false
    for (col = 0;col <= 2; col++){
      for (row = 0;row <= 2;row++){
        if (values[row][col] == 0){
          foundEmpty = true
        }
      }
    }
    if (foundEmpty){
      return null
    }
    return TIE
  }
  makeMove(row, column){
    if(this.state.winner){
      return
    }
    if (this.state.values[row][column] == 0){
      var symb = 0
      var nextPlayer = ""
      if(this.state.turn == "player1"){
        symb = 1
        nextPlayer = "player2"
      }else{
        symb = 2
        nextPlayer = "player1"
      }
      let values = this.state.values
      values[row][column] = symb
      let winner = this.checkWinner(values)
      this.setState({"values": values, "turn":nextPlayer, "winner":winner})
    }
  }
  render(){
    return (
      <Board player1={this.state.player1} turn={this.state.turn} player2={this.state.player2} winner={this.state.winner} values={this.state.values} makeMove={this.makeMove} reset={this.reset}/>
    )
  }
}

const WinnerDisplay = ({winner, reset, playerInfos})=>{
  const resetButton = <button styleName="playbutton" onClick={reset}>Neues Spiel</button>
  if(winner === TIE){
    return <div styleName="winner-display">Unentschieden! <div>{resetButton}</div></div> 
  }
  
  if(winner !== null){
    let winnerName = players[winner]
    return <div styleName="winner-display">Sieger: {playerInfos[winnerName].name} <div>{resetButton}</div></div>
  }
  return null
}

WinnerDisplay.propTypes = {
  winner: PropTypes.number,
  reset: PropTypes.func,
  playerInfos: PropTypes.object,
}



export class OnlineBoardController extends React.Component{
  constructor(props){
    super(props)
    this.makeMove = this.makeMove.bind(this)
    this.checkWinner = this.checkWinner.bind(this)
    this.reset = this.reset.bind(this)
    this.joinGame = this.joinGame.bind(this)
    this.setGameName = this.setGameName.bind(this)
    this.setPlayerName = this.setPlayerName.bind(this)
    this.gotNewMove = this.gotNewMove.bind(this)
    this.loadedAvailableGames = this.loadedAvailableGames.bind(this)
    this.renderSelect = this.renderSelect.bind(this)
    this.state = Object.assign({}, BoardStore.getState(), {me:null, gameName:null, available_games:[]})
  }
  componentDidMount(){
    BoardStore.listen(this.gotNewMove)
    GameStore.listen(this.loadedAvailableGames)
    GameActions.loadAvailableGames()
  }
  componentWillUnmount(){
    BoardStore.unlisten(this.gotNewMove)
    GameStore.unlisten(this.loadedAvailableGames)
    BoardActions.leaveGame()
  }
  gotNewMove(state){
    this.setState(state)
  }
  loadedAvailableGames(state){
    this.setState(state)
  }
  reset(){
    BoardActions.resetGame(this.state.gameName)
  }
  checkWinner(values){
    //Rows prüfen
    for (var row = 0;row <= 2; row++){
      var lastValue = null
      for (var col = 0;col <= 2;col++){
        var currentValue = values[row][col]
        if(lastValue === null){
          if(currentValue === 0){
            break
          }else{
            lastValue = currentValue
          }
        }else{
          if (currentValue !== lastValue){
            break
          }
          if (col == 2){
            return currentValue
          }
        }
      }
    }
    //Cols Prüfen
    for (col = 0;col <= 2; col++){
      lastValue = null
      for (row = 0;row <= 2;row++){
        currentValue = values[row][col]
        if(lastValue === null){
          if(currentValue === 0){
            break
          }else{
            lastValue = currentValue
          }
        }else{
          if (currentValue !== lastValue){
            break
          }
          if (row == 2){
            return currentValue
          }
        }
      }
    }
    // Über kreuz
    if (values[1][1] !== 0){
      if(values[0][0] === values[1][1] && values[1][1] === values[2][2]){
        return values[1][1]
      }
      if(values[2][0] === values[1][1] && values[1][1] === values[0][2]){
        return values[1][1]
      }
    }
    // Unentschieden ?
    let foundEmpty = false
    for (col = 0;col <= 2; col++){
      for (row = 0;row <= 2;row++){
        if (values[row][col] == 0){
          foundEmpty = true
        }
      }
    }
    if (foundEmpty){
      return null
    }
    return TIE
  }
  makeMove(row, column){
    if(this.state.winner){
      return
    }
    if (this.state[this.state.turn].name === this.state.me){
      if (this.state.values[row][column] == 0){
        var symb = 0
        var nextPlayer = ""
        if(this.state.turn == "player1"){
          symb = 1
          nextPlayer = "player2"
        }else{
          symb = 2
          nextPlayer = "player1"
        }
        let values = this.state.values
        values[row][column] = symb
        let winner = this.checkWinner(values)
        BoardActions.makeMove(Object.assign({}, this.state, {"values": values, "turn":nextPlayer, "winner":winner}))
        this.setState({"turn":nextPlayer})
      }
      
    }
  }
  setGameName(ev){
    let gameName = ev.currentTarget.value
    this.setState({"gameName":gameName})
  }
  setPlayerName(ev){
    let playerName = ev.currentTarget.value
    this.setState({"me":playerName})
  }
  joinGame(){
    BoardActions.createGame(this.state.gameName, this.state.me)
    
  }
  renderSelect(){
    var options = [<option value=""></option>]
    for (let game of this.state.available_games){
      options.push(<option value={game}>{game}</option>)
    }
    return options
  }
  render(){
    if(!this.state.player1 && !this.state.player2){
      return (
        <div styleName="gameselect">
          <div>{this.state.error}</div>
            <div styleName="inputrow">
              <label> <span styleName="label">Spiel:</span> <input type="text" onChange={this.setGameName} value={this.state.gameName} placeholder="Spiel"/></label>
            </div>
            <div styleName="inputrow">
              <label> <span styleName="label">Vorhandene Spiele:</span> <select onChange={this.setGameName} value={this.state.gameName}>{this.renderSelect()}</select></label>
            </div>
            <div styleName="inputrow">
              <label> <span styleName="label">Spielername:</span> <input type="text" onChange={this.setPlayerName} value={this.state.me}  placeholder="Spielername"/></label>
            </div>
          <button disabled={this.state.isJoining} styleName="playbutton" onClick={()=>{this.joinGame()}}>Beitreten</button>
        </div>
      )
    }

    if(!this.state.player2 || !this.state.player1){
      return (
        <div>
          Warte auf 2. Spieler...
        </div>
      )
    }
    return (
      <Board player1={this.state.player1} turn={this.state.turn} player2={this.state.player2} winner={this.state.winner} values={this.state.values} makeMove={this.makeMove} reset={this.reset}/>
    )
  }
}

class Board extends React.Component{
  static propTypes={
    player1: PropTypes.object,
    player2: PropTypes.object,
    winner: PropTypes.string,
    reset: PropTypes.func,
    makeMove: PropTypes.func,
    values: PropTypes.array,
    turn: PropTypes.string,
  }
  constructor(props){
    super(props)
    this.state = {"fadeIn":false}
  }
  componentDidMount(){
    setTimeout(()=>{
      this.setState({"fadeIn":true})
    }, 200)
  }
  render(){
    let styleName = this.state.fadeIn ? "board-fade-in" : "board"
    return (
      <div styleName={styleName}>
        <div styleName="player-info">{this.props.player1.name} gegen {this.props.player2.name}</div>
        <div>am Zug: {this.props[this.props.turn].name}</div>

        <div>
          <BoardCell onClick={()=>{this.props.makeMove(0, 0)}} value={this.props.values[0][0]}/><BoardCell onClick={()=>{this.props.makeMove(0, 1)}} value={this.props.values[0][1]}/><BoardCell onClick={()=>{this.props.makeMove(0, 2)}} value={this.props.values[0][2]}/>
        </div>
        <div>
          <BoardCell onClick={()=>{this.props.makeMove(1, 0)}} value={this.props.values[1][0]}/><BoardCell onClick={()=>{this.props.makeMove(1, 1)}} value={this.props.values[1][1]}/><BoardCell onClick={()=>{this.props.makeMove(1, 2)}}  value={this.props.values[1][2]}/>
        </div>
        <div>
          <BoardCell onClick={()=>{this.props.makeMove(2, 0)}} value={this.props.values[2][0]}/><BoardCell onClick={()=>{this.props.makeMove(2, 1)}} value={this.props.values[2][1]}/><BoardCell onClick={()=>{this.props.makeMove(2, 2)}} value={this.props.values[2][2]}/>
        </div>
        {<WinnerDisplay winner={this.props.winner} reset={this.props.reset} playerInfos={{"player1":this.props.player1, "player2":this.props.player2}}/>}
      </div>
    )
  }
}

class BoardCell extends React.Component{
  static propTypes={
    value: PropTypes.Number,
    onClick: PropTypes.func,
  }
  constructor(props){
    super(props)
    this.renderValue = this.renderValue.bind(this)
  }
  renderValue(){
    if(this.props.value == 0){
      return null
    }else if(this.props.value == 1){
      return "X"
    }else{
      return "O"
    }
  }

  render(){
    let value = this.renderValue()
    let styleName = "board-cell"
    if(value){
      styleName = "board-cell-done"
    }
    return (
      <div onClick={this.props.onClick} styleName={styleName}>{this.renderValue()}</div>
    )
  }
}


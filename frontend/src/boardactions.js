import alt from "./alt.js"
import request from "superagent"

var ws = null

function createWebSocket(path) {
  var protocolPrefix = (window.location.protocol === "https:") ? "wss:" : "ws:"
  return new WebSocket(protocolPrefix + "//" + location.host + path)
}

class BoardActions{
  makeMove(state){
    ws.send(JSON.stringify(state))
  }
  onError(response){
    return response.body
  }
  onMoveSuccess(response){

    return response.body
  }
  onWsMove(data){
    // Wir haben nun unsere ganze spiel logik offline,
    // diese könnten wir für die online version auch im server machen,
    // es ist nämlich auf diese art sehr einfach zu betrügen
    var gameData = JSON.parse(data.data)
    if(gameData.event_type && gameData.event_type == "GameClose"){
      this.resetGame()
      return false
    }
    return gameData
  }
  createGame(name, playername){
    return request.get(`/api/newgame/${name}/`).end((err, response)=>{
      if(err){
        this.onError(response)
      }else{
        this.onMoveSuccess(response)
        this.joinGame(name, playername)
      }
    })
  }
  resetGame(name){
    request.delete(`/api/game/${name}/`).end()
    ws.close()
    return true
  }
  joinGame(name, playername){
    return request.get(`/api/joingame/${name}/${playername}`).end((err, response)=>{
      if(err){
        this.onError(response)
      }else{

        ws = createWebSocket("/ws/" + name + "/" + playername )
        ws.onmessage = this.onWsMove
        ws.onopen = function(){
          ws.send(JSON.stringify(response.body))
        }
        this.onMoveSuccess(response)
      }
    })
  }
  leaveGame(){
    ws.close()
    return true
  }
}

export default alt.createActions(BoardActions)


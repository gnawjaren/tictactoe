import alt from "./alt.js"
import request from "superagent"

class GameActions{
  loadAvailableGames(){
    request.get("/api/availablegames").end((err, response)=>{
      this.loadedAvailableGames(response)
    })
  }
  loadedAvailableGames(response){
    console.log(response)
    return response.body
  }
}

export default alt.createActions(GameActions)
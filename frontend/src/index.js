import React from "react"
import ReactDOM from "react-dom"
import "./styles.css"
import { Router, Route, browserHistory, Link } from "react-router"

import OfflineBoardController, {OnlineBoardController} from "./board"

/**
 * Index lässt uns nur auswählen - ob wir ein online
 * oder ein offline spiel machen möchten
 */
class Index extends React.Component{
  render(){
    return (
      <div styleName="index">
        <div styleName="header">
          Play some <span>TicTacToe</span> online!
        </div>
        <Link to="/offline"><button styleName="playbutton">Offline Spielen</button></Link>
        <Link to="/online"><button styleName="playbutton">Online Spielen</button></Link>
      </div>
    )
  } 
}
 
// Wir rendern unseren router
ReactDOM.render(
    <div styleName="game">
      <Router history={browserHistory}>
        <Route path="/index.html" component={Index}/>
        <Route path="/" component={Index}/>
        <Route path="/offline" component={OfflineBoardController}/>
        <Route path="/online" component={OnlineBoardController}/>
      </Router>
    </div>,
    document.getElementById("app")
)
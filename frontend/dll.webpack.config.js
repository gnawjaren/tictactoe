/*eslint no-undef: 0*/
var webpack = require("webpack")
var HtmlWebpackPlugin = require("html-webpack-plugin")
var isProd = (process.env.NODE_ENV === "production")

function getPlugins(){
  let plugins = [
    new webpack.DllPlugin({
      // The path to the manifest file which maps between
      // modules included in a bundle and the internal IDs
      // within that bundle
      path: __dirname + "/build/[name]-manifest.json",
      // The name of the global variable which the library"s
      // require function has been assigned to. This must match the
      // output.library option above
      name: "[name]_lib"
    }),
    new HtmlWebpackPlugin({  // Also generate a test.html
      template: __dirname + "/index.html",
      filename: __dirname + "/index.1.html"
    }),
  ]
  if(isProd){
    plugins.push(new webpack.DefinePlugin({
      "process.env":{
        "NODE_ENV": JSON.stringify("production"),
      },
      "__DEV__":false
    }))
    plugins.push(
      new webpack.optimize.UglifyJsPlugin({
        compress: {
          warnings: false,
          screw_ie8: true,
          conditionals: true,
          unused: true,
          comparisons: true,
          sequences: true,
          dead_code: true,
          evaluate: true,
          if_return: true,
          join_vars: true,
        },
        output: {
          comments: false,
        },
        sourceMap: true
      })
    )
  }
  return plugins
}

module.exports = {
  context: __dirname,
  entry: {
    // create two library bundles, one with jQuery and
    // another with Angular and related libraries
    "vendor": [
      "babel-polyfill",
      "react",
      "react-dom",
      "react-router",
      "alt",
    ]
  },

  output: {
    filename: isProd ? "[name].bundle.[hash].js" : "[name].bundle.js",
    path: __dirname + "/build",
    publicPath: "/static/",
    // The name of the global variable which the library"s
    // require() function will be assigned to
    library: "[name]_lib",
  },
  stats: {
    // Add asset Information
    assets: false,
    // Sort assets by a field
    assetsSort: "field",
    // Add information about cached (not built) modules
    cached: false,
    // Show cached assets (setting this to `false` only shows emitted files)
    cachedAssets: false,
    // Add children information
    children: false,
    // Add chunk information (setting this to `false` allows for a less verbose output)
    chunks: false,
    // Add built modules information to chunk information
    chunkModules: false,
    // Add the origins of chunks and chunk merging info
    chunkOrigins: false,
    // `webpack --colors` equivalent
    colors: true,
    // Display the distance from the entry point for each module
    depth: false,
    // Display the entry points with the corresponding bundles
    entrypoints: false,
    // Add errors
    errors: true,
    // Add details to errors (like resolving log)
    errorDetails: true,
    // Exclude modules which match one of the given strings or regular expressions
    exclude: [],
    // Add the hash of the compilation
    hash: true,
    // Set the maximum number of modules to be shown
    maxModules: 15,
    // Add built modules information
    modules: false,
    // Sort the modules by a field
    modulesSort: "field",
    // Show performance hint when file size exceeds `performance.maxAssetSize`
    performance: false,
    // Show the exports of the modules
    providedExports: false,
    // Add public path information
    publicPath: false,
    // Add information about the reasons why modules are included
    reasons: false,
    // Add the source code of modules
    source: false,
    // Add timing information
    timings: true,
    // Show which exports of a module are used
    usedExports: false,
    // Add webpack version information
    version: false,
    // Add warnings
    warnings: true
  },

  plugins: getPlugins()
}
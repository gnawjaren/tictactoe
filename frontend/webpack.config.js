/*eslint no-undef: 0*/
var path = require("path")
var webpack = require("webpack")
var ExtractTextPlugin = require("extract-text-webpack-plugin")
var HtmlWebpackPlugin = require("html-webpack-plugin")
var CopyWebpackPlugin = require("copy-webpack-plugin")


var isProd = (process.env.NODE_ENV === "production")
function getPlugins(){
  var plugins = [
    new ExtractTextPlugin({
      filename:"master.css",
      allChunks: true
    }),
    new CopyWebpackPlugin([{"from":"assets/fonts", "to":"fonts"}, {"from":"assets/images", "to":"images"}], {}),
    new HtmlWebpackPlugin({
      template: "index.1.html"
    }),
    new webpack.DllReferencePlugin({
      context: ".",
      manifest: require("./build/vendor-manifest.json")
    })

  ]
  if(isProd){
    plugins.push(new webpack.DefinePlugin({
      "process.env":{
        "NODE_ENV": JSON.stringify("production"),
      },
      "__DEV__":false
    }))
    plugins.push(
      new webpack.optimize.UglifyJsPlugin({
        compress: {
          warnings: false,
          screw_ie8: true,
          conditionals: true,
          unused: true,
          comparisons: true,
          sequences: true,
          dead_code: true,
          evaluate: true,
          if_return: true,
          join_vars: true,
        },
        output: {
          comments: false,
        },
        sourceMap: true
      })
    )
  }else{
    plugins.push(new webpack.DefinePlugin({
      "__DEV__":true
    }))
  }
  return plugins

}


module.exports = function(env){
  return {
    cache: true,
    context: __dirname,
    entry: {
      "app":[
        "./src/index.js"
      ],
    },
    stats: {
      assets: false,
      assetsSort: "field",
      cached: false,
      cachedAssets: false,
      children: false,
      chunks: false,
      chunkModules: true,
      chunkOrigins: false,
      colors: true,
      depth: false,
      entrypoints: false,
      errors: true,
      errorDetails: true,
      exclude: [],
      hash: true,
      maxModules: 15,
      modules: false,
      modulesSort: "field",
      performance: false,
      providedExports: false,
      publicPath: false,
      reasons: false,
      source: false,
      timings: true,
      usedExports: false,
      version: false,
      warnings: true
    },
    devtool: (isProd || (env && env.debug)) ? "source-map" : "inline-eval-cheap-source-map",
    output: {
      path: __dirname + "/build",
      publicPath: "/static/",
      filename: isProd ? "[name].bundle.[hash].js" : "[name].bundle.js",
      chunkFilename: isProd ? "[id].bundle.[hash].js" : "[id].bundle.js",
    },
    module: {
      rules: [
        {
          test: path.join(__dirname, "src"),
          exclude: /node_modules/,
          loader: "babel-loader",
          options:{
            babelrc: false,
            presets: ["react", [ "es2015", { modules: false } ]],
            cacheDirectory: true,
            plugins: [
              "transform-export-extensions",
              "transform-decorators-legacy",
              "transform-class-properties",
              "transform-object-rest-spread",
              "transform-object-assign",
              "syntax-trailing-function-commas",
              ["react-css-modules", {context:__dirname}],
            ],
            retainLines: true,
            sourceMaps: true
          }
        },
        {
          test: /\.css$/,
          include: [
            /node_modules/,
          ],
          use: ExtractTextPlugin.extract({
            fallback:"style-loader", 
            use:[
              {
                loader: "css-loader",
              },
              {
                loader: "postcss-loader"
              }
            ]})

        },
        {
          test: /\.css$/,
          exclude: [
            /node_modules/,
          ],
          use: ExtractTextPlugin.extract({
            fallback:"style-loader",
            use:[
              {
                loader: "css-loader",
                options: {
                  context: __dirname,
                  sourceMap: true,
                  importLoaders: 1,
                  modules: true,
                  localIdentName: "[path]___[name]__[local]___[hash:base64:5]"
                }
              },
              {
                loader: "postcss-loader"
              }
            ]
          })
        },
        {
          test: /\.(png|woff|woff2|eot|ttf|svg)$/, loader: "url-loader?limit=100000"
        }
      ]
    },
    resolve: {

      alias:{
        app:  __dirname + "/src",
      }
    },
    plugins: getPlugins()
  }
}

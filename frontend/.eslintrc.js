module.exports = {
    "parser": "babel-eslint",
    "env": {
        "browser": true,
        "es6": true
    },
    "parserOptions": {
        "ecmaFeatures": {
            "experimentalObjectRestSpread": true,
            "jsx": true
        },
        "sourceType": "module"
    },
    "plugins": [
        "react",
    ],
    "extends": ["eslint:recommended", "plugin:react/recommended"],
    "rules": {
        "indent": [
            "error",
            2
        ],
        "linebreak-style": [
            "error",
            "unix"
        ],
        "quotes": [
            "error",
            "double",
        ],
        "semi": [
            "error",
            "never"
        ],
        "space-infix-ops": ["error", {"int32Hint": true}],
        "no-unused-vars": ["error", { "ignoreRestSiblings": true }]
    },
    "globals": {
        "it": true,
        "describe": true,
        "beforeEach": true,
        "afterEach": true,
        "expect": true,
        "require": true,
    }
}

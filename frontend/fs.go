package frontend

import (
	"github.com/elazarl/go-bindata-assetfs"
	"github.com/gin-gonic/gin"
	logging "github.com/op/go-logging"
)

var logger = logging.MustGetLogger("frontend")

func StaticFiles() *assetfs.AssetFS {
	return &assetfs.AssetFS{Asset: Asset, AssetDir: AssetDir, AssetInfo: AssetInfo, Prefix: ""}
}

func IndexFile() gin.HandlerFunc {
	return func(c *gin.Context) {
		var err error
		var data []byte
		data, err = Asset("index.html")
		if err != nil {
			c.Data(404, "text/html", nil)
		}
		c.Data(200, "text/html", data)
	}
}

require 'rake/clean'

appName = "tictactoe"
appPath = "gitlab.com/gnawjaren/#{appName}"

# Alle .go source files
goFiles = Rake::FileList["**/*.go"]

# Alle js/css/html source files
jsFiles = Rake::FileList["frontend/src/**/*.js", "frontend/src/**/*.css", "frontend/src/**/*.html"]

# Alle webpack build files in frontend/build
jsBuildFiles = Rake::FileList["frontend/build/**"]

jsBin = "frontend/node_modules/.bin"

# Alle hilfe source files
helpFiles = Rake::FileList["help/src/**/*.md"]

# Alle bindata.go files
bindataGoFiles = Rake::FileList["**/bindata.go"]

# Templates
templateFiles = Rake::FileList["web/print/templates/**/*.html"]

# Generierte index.html in frontend/build
outIndexHtml = "frontend/build/index.html"

# Generierte index.1.html mit vendor.bundle
outIndex1Html = "frontend/index.1.html"

# Source index.html
baseIndexHtml = "frontend/index.html"

# Webpack Konfigurationsdatei
webPackConfig = "frontend/webpack.config.js"

# Webpack Konfigurationsdatei für Dll Bundle
webPackDllConfig = "frontend/dll.webpack.config.js"

# alle dateien in node_modules
nodeModules = Rake::FileList["frontend/node_modules/**"]

# package.json
packageJson = "frontend/package.json"

# Module um OS herauszufinden
module OS
  def OS.windows?
    (/cygwin|mswin|mingw|bccwin|wince|emx/ =~ RUBY_PLATFORM) != nil
  end

  def OS.mac?
   (/darwin/ =~ RUBY_PLATFORM) != nil
  end

  def OS.unix?
    !OS.windows?
  end

  def OS.linux?
    OS.unix? and not OS.mac?
  end
end

# Procuction Build wird über NODE_ENV=production gesteuert
if ENV['NODE_ENV'] == "production"
    BINDATA_ARGS = ""
else
    BINDATA_ARGS = "-debug"
end

if ENV['debug'] == "true"
    WEBPACK_ARGS = "--env.debug"
else
    WEBPACK_ARGS = ""
end

if OS.windows? 
    app = "#{appName}.exe"
    goBindataBin = "../../../../bin/go-bindata.exe"
    osVerCmd = "ver"
else
    app = appName
    goBindataBin = "../../../../bin/go-bindata"
    osVerCmd = "uname -a"
end

if OS.mac?
    goCmdArgs = "-ldflags=-s"
else
    goCmdArgs = ""
end

appGoPath = "../../../../bin/#{app}"

task default: ["install"]

desc("Startet #{appName} nachdem der Compile komplett ist")
task :run => ["../../../../bin/#{app}"] do |t|
    sh "../../../../bin/#{app}"
end

desc("go install #{appPath}")
task install: ["../../../../bin/#{app}"]

desc("go build #{appPath}")
task build: [app]

desc("go-bindata ...")
task bindata: ["frontend/bindata.go"]

desc("webpack")
task webpack: [outIndexHtml]

desc("watch")
task :watch => [outIndex1Html] do |t|
    sh "#{jsBin}/webpack --config #{webPackConfig} #{WEBPACK_ARGS} --watch"
end

file app => [*goFiles, "frontend/bindata.go"] do |t|
    sh "go build #{goCmdArgs} #{appPath}"
end

file "../../../../bin/#{app}" => [*goFiles,  "frontend/bindata.go"] do |t|
    sh "go install #{goCmdArgs} #{appPath}"
end

file "frontend/bindata.go" => [*jsBuildFiles, outIndexHtml, goBindataBin] do |t|
    sh "cd frontend && go-bindata #{BINDATA_ARGS} -pkg frontend -prefix build/ build/..."
end

file outIndexHtml => [*jsFiles, outIndex1Html, webPackConfig] do |t|
    sh "#{jsBin}/webpack --config #{webPackConfig} #{WEBPACK_ARGS}"
end

# Html File mit vendor.bundle.js
file outIndex1Html => [*nodeModules, webPackDllConfig, "frontend/node_modules"] do |t|
    sh "#{jsBin}/webpack --config #{webPackDllConfig} #{WEBPACK_ARGS}"
end

desc("Run Npm Install")
task :npmInstall => [packageJson] do |t|
    sh "cd frontend && npm install"
end

file "frontend/node_modules" do |t|
    sh "cd frontend && npm install"
end

file goBindataBin do |t| 
    sh "go get -u github.com/jteeuwen/go-bindata/..."
end

task :eslint do |t|
    sh "cd frontend && eslint -c frontend/.eslintrc.js frontend/src/"
end

task :jest do |t|
    sh "#{jsBin}/jest --config frontend/.jestconfig.json"
end

CLEAN.include(*jsBuildFiles)
CLEAN.include(outIndex1Html)
CLEAN.include(bindataGoFiles)
CLEAN.include(app)
CLEAN.include(appGoPath)

CLOBBER << "frontend/node_modules"
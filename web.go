package main

import (
	"encoding/json"
	"fmt"
	"strings"

	"github.com/gin-gonic/gin"
	melody "gopkg.in/olahol/melody.v1"
)

/*
Web repräsentiert unsere handler
*/
type Web struct {
	App *App
}

/*
NewGame erstellt ein neues Spiel
*/
func (w *Web) NewGame(c *gin.Context) {
	GameName := c.Param("gamename")
	game := w.App.NewGame(GameName)
	c.JSON(200, game)
}

/*
GameStatus gibt uns den Status eines Spiels zurück
*/
func (w *Web) GameStatus(c *gin.Context) {
	GameName := c.Param("gamename")
	game, err := w.App.GameByName(GameName)
	if err != nil {
		c.JSON(500, JSONError{err.Error()})
		return
	}
	c.JSON(200, game)
}

/*
JoinGame lässt uns einem Spiel beitreten
*/
func (w *Web) JoinGame(c *gin.Context) {
	GameName := c.Param("gamename")
	PlayerName := c.Param("playername")
	game, err := w.App.GameByName(GameName)
	if err != nil {
		c.JSON(500, JSONError{err.Error()})
		return
	}
	err = game.JoinGame(&Player{PlayerName})
	if err != nil {
		c.JSON(500, JSONError{err.Error()})
		return
	}
	c.JSON(200, game)
}

/*
LeaveGame - ein spiel verlassen
*/
func (w *Web) LeaveGame(c *gin.Context) {
	GameName := c.Param("gamename")
	PlayerName := c.Param("playername")
	game, err := w.App.GameByName(GameName)
	if err != nil {
		c.JSON(500, JSONError{err.Error()})
		return
	}
	err = game.LeaveGame(&Player{PlayerName})
	if err != nil {
		c.JSON(500, JSONError{err.Error()})
		return
	}
	if game.IsEmpty() {
		fmt.Println("Keiner mehr drin, lösche spiel!")
		w.App.DeleteGameByName(GameName)
	}
	c.JSON(200, game)
}

/*
DeleteGame - ein spiel verlassen
*/
func (w *Web) DeleteGame(c *gin.Context) {
	GameName := c.Param("gamename")
	w.App.DeleteGameByName(GameName)
	event := GameEvent{"GameClose"}
	eventData, _ := json.Marshal(event)
	w.App.Melody.BroadcastFilter(eventData, func(q *melody.Session) bool {
		game1 := GameName
		game2 := strings.Split(q.Request.URL.Path, "/")[2]
		return game1 == game2
	})
	c.String(200, "")
}

/*
MakeMove - Einen zug machen
(benutzen wir allerdings gar nicht - läuft über unseren websocket)
*/
func (w *Web) MakeMove(c *gin.Context) {
	GameName := c.Param("gamename")
	//game, err := w.App.GameByName(GameName)
	var game Game
	var err error
	if err = c.Bind(&game); err == nil {
		w.App.Games[GameName] = &game
		c.JSON(200, game)
	}
	c.JSON(500, JSONError{err.Error()})
}

/*
JSONError repräsentiert einen Fehler den wir über http ausgeben
*/
type JSONError struct {
	Message string
}

/*
GameEvent repräsentiert ein event - verwenden wir nur für GameClose
*/
type GameEvent struct {
	EventType string `json:"event_type"`
}

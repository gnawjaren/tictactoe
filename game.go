package main

import "errors"

/*
Game repräsentiert ein laufendes Spiel
*/
type Game struct {
	Values  [][]int `json:"values"`
	Player1 *Player `json:"player1"`
	Player2 *Player `json:"player2"`
	Winner  *string `json:"winner"`
	Turn    string  `json:"turn"`
}

/*
IsEmpty prüft ob noch spieler im Spiel sind
*/
func (g *Game) IsEmpty() bool {
	return g.Player1 == nil && g.Player2 == nil
}

/*
IsFree prüft noch ein platz frei ist
*/
func (g *Game) IsFree() bool {
	return g.Player1 == nil || g.Player2 == nil
}

/*
JoinGame - diesem Spiel beitreten
*/
func (g *Game) JoinGame(player *Player) error {
	if g.Player1 == nil {
		if g.Player2 != nil && player.Name == g.Player2.Name {
			return errors.New("Name bereits vergeben!")
		}
		g.Player1 = player
		return nil
	}

	if g.Player2 == nil {
		if player.Name == g.Player1.Name {
			return errors.New("Name bereits vergeben!")
		}
		g.Player2 = player
		return nil
	}

	return errors.New("Already 2 Players in Game!")
}

/*
LeaveGame - dieses Spiel verlassen
*/
func (g *Game) LeaveGame(player *Player) error {
	if g.Player1 != nil && g.Player1.Name == player.Name {
		g.Player1 = nil
		return nil
	}

	if g.Player2 != nil && g.Player2.Name == player.Name {
		g.Player2 = nil
		return nil
	}

	return errors.New("Already 2 Players in Game!")
}
